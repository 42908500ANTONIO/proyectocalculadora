/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.Calculo;
import root.model.respuestaFinal;

/**
 *
 * @author estay
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         
      
      
      //int =resultado.calculoInteres(Integer.parseInt(capital),Integer.parseInt(tasa), Integer.parseInt(tiempo)) ;
      
           
      
      //int r=Integer.parseInt(capital)*(Integer.parseInt(tasa))/100*Integer.parseInt(tiempo);
     
      
       //request.getRequestDispatcher("salida.jsp");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> El resultado del interes es:  </h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
                    
      String capital= request.getParameter("capital");
      String tasa= request.getParameter("tasa");
      String tiempo= request.getParameter("tiempo");
   
      Calculo resultado= new Calculo();
      int cap=resultado.calculoCapital(Integer.parseInt(capital));
      int tas=resultado.calculoTasa(Integer.parseInt(tasa));
      int ti=resultado.calculoTiempo(Integer.parseInt(tiempo));
      
      int interes1=cap*tas;
      int interes2=interes1/100;
      int interes3=interes2*ti;
        
      respuestaFinal res=new respuestaFinal();  
      res.setInteresFinal(interes3);
      
      request.setAttribute("res", res);
      
      request.getRequestDispatcher("salida.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
