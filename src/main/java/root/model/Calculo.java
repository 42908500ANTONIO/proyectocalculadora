
package root.model;


public class Calculo {
    
private int capital;
private int tasa;
private int tiempo;


public int calculoCapital(int capital){
return capital;
}

public int calculoTasa(int tasa){
return tasa;
}

public int calculoTiempo(int tiempo){
return tiempo;
}


public int calculoInteres(int capital, int tasa, int tiempo){
return capital*(tasa/100)*tiempo;
}



    public int getCapital() {
    return capital;
    }

    
    public void setCapital(int capital) {
        this.capital = capital;
    }

   
    public int getTasa() {
        return tasa;
    }

    
    public void setTasa(int tasa) {
        this.tasa = tasa;
    }

   
    public int getTiempo() {
        return tiempo;
    }

    
    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

   

    
    
}
